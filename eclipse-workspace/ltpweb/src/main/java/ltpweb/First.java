package ltpweb;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/ops")
public class First extends HttpServlet {
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		PrintWriter out = resp.getWriter();
		
		out.println("<html>");
		out.println("<head>");
		out.println("</head>");
		out.println("<body>");
		out.println("<h1>M�todo HTTP POST</h1>");
		out.println("<h3>Recebi seus dados...</h3>");
		out.println("<h1>Usu�rio: " + req.getParameter("name"));
		out.println("<h1>Senha: " + req.getParameter("password"));
		out.println("</body>");
		out.println("</html>");
	}
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		PrintWriter out = resp.getWriter();
		
		out.println("<html>");
		out.println("<head>");
		out.println("</head>");
		out.println("<body>");
		out.println("<h1>M�todo HTTP GET</h1>");
		out.println("<h1>Seja bem vindo, " + req.getParameter("nome") + " " + req.getParameter("sobrenome") + "</h1>");
		out.println("</body>");
		out.println("</html>");
	}

}
