package ltpweb;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/cicloVida")
public class CicloVida extends HttpServlet{
	
	private String name;
	private String password;
	
	@Override
	public void init() throws ServletException {
		System.out.println("Servlet Iniciado!");
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		PrintWriter out = resp.getWriter();
		name = "Ygor";
		password = "ygor123";
		
		int contadorLoginSucesso = 0;
		int contadorLoginFalha = 0;
		
		if(name == req.getParameter("name") && password == req.getParameter("password")) {
			++contadorLoginSucesso;
			out.println("<html>");
			out.println("<head>");
			out.println("</head>");
			out.println("<body>");
			out.println("<h1>Sucesso: " + contadorLoginSucesso + "</h1>");
			out.println("</body>");
			out.println("</html>");
		} else {
			++contadorLoginFalha;
			out.println("<html>");
			out.println("<head>");
			out.println("</head>");
			out.println("<body>");
			out.println("<h1>Falha: " + contadorLoginFalha + "</h1>");
			out.println("</body>");
			out.println("</html>");
		}
	}
	
	@Override
	public void destroy() {
		System.out.println("Servlet Destru�do!");
	}

}
